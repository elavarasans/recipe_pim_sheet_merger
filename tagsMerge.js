const excel = require("exceljs");
const fs = require("fs");
const path = require("path");

const tagsSheetPath = path.resolve("input/tags/RecipeTags.xlsx");
const enProductSheetPath = path.resolve("input/en/productmaster");
const frProductSheetPath = path.resolve("input/fr/productmaster");
const enProducts = [];
const frProducts = [];
const enTagsSheetProps = {};
const frTagsSheetProps = {};

const enFileSavePath = path.resolve("output/tags/EnProductProps_" + new Date().getTime() + ".xlsx");
const frFileSavePath = path.resolve("output/tags/FrProductProps_" + new Date().getTime() + ".xlsx");
const enWorkbook = new excel.Workbook();
const frWorkbook = new excel.Workbook();

const facetNameReplaceJson = {
    "Ingredients": "ingredientname",
    "Course": "courses",
    "Difficulty Level": "difficultylevel",
    "Kraft Products": "kraftproducts",
    "Lifestyle": "lifestyle",
    "Cooking Method": "cookingmethod",
    "Health & Diet": "healthdiet",
    "Occasion/Theme": "occasiontheme",
    "Type of Dish": "dishtype",
    "Season": "season",
    "Meal": "meal",
    "Cuisine": "cuisine",
    "Type de plat": "dishtype",
    "Style de vie": "lifestyle",
    "Degré de difficulté": "difficultylevel",
    "Mode de préparation": "cookingmethod",
    "Occasion ou thème": "occasiontheme",
    "Saison": "season"
};

const totalcount = (locale) => {
    let filesPath = locale == "en_CA" ? enProductSheetPath : frProductSheetPath;
    return fs.readdirSync(filesPath).length;
}

(async() => {
    let a = await processProductSheet("en_CA");
    console.log("Total En products: " + enProducts.length);
    let b = await processProductSheet("fr_CA");
    console.log("Total Fr products: " + frProducts.length);

    let c = await processOnRecipeTags();
    console.log("En props count: ", Object.keys(enTagsSheetProps).length);
    console.log("Fr props count: ", Object.keys(frTagsSheetProps).length);

    generateSheets();

})();

function processOnRecipeTags() {
    return new Promise(resolve => {
        let tempbook = new excel.Workbook();
        tempbook.xlsx.readFile(tagsSheetPath).then(book => {
            let recipeTagsSheet = book.getWorksheet("RecipeTags");
            recipeTagsSheet.spliceRows(1, 1);
            recipeTagsSheet.eachRow((row, rno) => {
                let langid = row.getCell(4).value.toString();
                let productid = row.getCell(5).value.toString();
                let facetname = row.getCell(7).value != null ? row.getCell(7).value.toString() : "";
                if (facetname != "") {
                    if (typeof facetNameReplaceJson[facetname] != "undefined") {
                        facetname = facetNameReplaceJson[facetname];
                    } else {
                        throw new Error(`Facet name replace key[${facetname}] not find`);
                    }

                    let facetvalue = row.getCell(8).value.toString();
                    if (langid == "1") {
                        if (enProducts.indexOf(productid) > -1) {
                            if (typeof enTagsSheetProps[productid] != "undefined") {
                                let propObj = enTagsSheetProps[productid];
                                if (typeof propObj[facetname] != "undefined") {
                                    propObj[facetname] =
                                        propObj[facetname] + "," + facetvalue;
                                } else {
                                    propObj[facetname] = facetvalue;
                                }
                                enTagsSheetProps[productid] = propObj;
                            } else {
                                enTagsSheetProps[productid] = {
                                    [facetname]: facetvalue
                                };
                            }
                        }
                    }
                    if (langid == "3") {
                        if (frProducts.indexOf(productid) > -1) {
                            if (typeof frTagsSheetProps[productid] != "undefined") {
                                let propObj = frTagsSheetProps[productid];
                                if (typeof propObj[facetname] != "undefined") {
                                    propObj[facetname] =
                                        propObj[facetname] + "," + facetvalue;
                                } else {
                                    propObj[facetname] = facetvalue;
                                }
                                frTagsSheetProps[productid] = propObj;
                            } else {
                                frTagsSheetProps[productid] = {
                                    [facetname]: facetvalue
                                };
                            }
                        }
                    }
                }
            });
            resolve(true);
        });
    });
}

function processProductSheet(locale) {
    return new Promise(resolve => {
        let keysLength = totalcount(locale);
        let index = 1;
        let productSheetPath = locale == "en_CA" ? enProductSheetPath : frProductSheetPath;
        if (fs.existsSync(productSheetPath) && fs.readdirSync(productSheetPath).length > 0) {
            fs.readdirSync(productSheetPath).forEach(fileName => {
                if (fileName.endsWith(".xlsx")) {
                    let tempbook = new excel.Workbook();
                    tempbook.xlsx.readFile(path.join(productSheetPath, fileName)).then(book => {
                        let productMasterSheet = book.getWorksheet("productmaster");

                        if (typeof productMasterSheet != "undefined") {
                            productMasterSheet.spliceRows(1, 1);
                            productMasterSheet.eachRow((row, rowno) => {
                                let productid = row.getCell(1).value.toString();
                                if (locale == "en_CA")
                                    enProducts.push(productid);
                                else
                                    frProducts.push(productid);
                            });
                        }
                        if (index == keysLength) {
                            resolve(true);
                            console.log(`\x1b[34mRead process ended for ${locale}\x1b[0m`);
                        }
                        index++;
                    });

                } else index++;
            });
        }
    });
}

function generateSheets() {
    const enSheet = initProductPropsSheet("en_CA");
    const frSheet = initProductPropsSheet("fr_CA");

    Object.keys(enTagsSheetProps).forEach((id) => {
        Object.keys(enTagsSheetProps[id]).forEach((facet) => {
            enSheet.addRow([
                id, facet, enTagsSheetProps[id][facet], "false", "en_CA", "true", "", ""
            ]);
        });
    });

    Object.keys(frTagsSheetProps).forEach((id) => {
        Object.keys(frTagsSheetProps[id]).forEach((facet) => {
            frSheet.addRow([
                id, facet, frTagsSheetProps[id][facet], "false", "fr_CA", "true", "", ""
            ]);
        });
    });

    enWorkbook.xlsx.writeFile(enFileSavePath);
    console.log(enFileSavePath + " created...");
    frWorkbook.xlsx.writeFile(frFileSavePath);
    console.log(frFileSavePath + " created...");

}

//Initiate productmasterproperties sheet
function initProductPropsSheet(locale) {
    const productPropsSheet = locale == "en_CA" ? enWorkbook.addWorksheet("productmasterproperties") : frWorkbook.addWorksheet("productmasterproperties");
    productPropsSheet.addRow([
        "productid", "name", "value", "locked", "locale", "overridebysku", "starttime", "endtime"
    ]);
    return productPropsSheet;

}