const excel = require("exceljs");
const fs = require("fs");
const path = require("path");

const defaultProps = {
    locale: "en_CA",
    defaultCategoryId: {
        en: "1000001",
        fr: "1000001"
    },
    catalogid: {
        en: "kraftcaen",
        fr: "kraftcafr"
    },
    additionalProductProperties: {
        languageid: "1",
        countryid:  "2",
        recipeindustrysector: "Retail"
    },
    startTime: "1517423400000",
    endTime: "1896114600000",
    productPropsRangePerSheet: 1000,
    isAltLangNeed: true
};

const enFilePath = {
    categoryPath: path.resolve("input/en/category"),
    productMasterPath: path.resolve("input/en/productmaster"),
    productPropsPath: path.resolve("input/en/productprops")
};

const frFilePath = {
    categoryPath: path.resolve("input/fr/category"),
    productMasterPath: path.resolve("input/fr/productmaster"),
    productPropsPath: path.resolve("input/fr/productprops")
};

// const categories = {};
var productcategory = {};
var products = [];
const productProps = {};
var altLangNameProperty = {};
var workbook = new excel.Workbook();
var productPropsWorkbook = new excel.Workbook();
const startTime = new Date().getTime();

const totalcount = (filesPath) => {
    // let filecount = 0;
    // let filesPath = defaultProps.locale == "en_CA" ? enFilePath : frFilePath;
    // Object.keys(filesPath).forEach(i => {
    //     let dirPath = filesPath[i];
    //     filecount += fs.readdirSync(dirPath).length;
    // });
    // return filecount;
    return fs.readdirSync(filesPath).length;
}

(() => {
    if (
        typeof defaultProps != "undefined" &&
        typeof defaultProps.locale != "undefined"
    ) {
        if (defaultProps.locale == "en_CA" || defaultProps.locale == "fr_CA") {
            console.log("\x1b[33mStart\x1b[0m");
            if(defaultProps.locale == "fr_CA") {
                defaultProps.additionalProductProperties.languageid = "3";
            }
            console.log("language id: " + defaultProps.additionalProductProperties.languageid);
            processOnSheets().then(() => {
                pruducts = products.sort((a, b) => a - b);

                console.log("Total products count: " + products.length);
                console.log("Total product props count: " + Object.keys(productProps).length);
                console.log("\x1b[35mSheet population started...\x1b[0m");
                generateProductSkuSheets();
                console.log("\x1b[35mProduct and sku masters completed...\x1b[0m");
                console.log("Workbook re-initialized for Productmasterprops");
                workbook = null;
                workbook = new excel.Workbook();
                // productcategory = null;
                generateProductPropsSheet();
                console.log("\x1b[35mProduct properties completed...\x1b[0m");
                console.log("\x1b[33mEnd\x1b[0m");
                // console.log(JSON.stringify(productProps["82613"]));
            });
        } else {
            console.log("\x1b[31mInvalid default locale...\x1b[0m");
        }
    } else {
        console.log("\x1b[31mDefault locale missing...\x1b[0m");
    }
})();

async function processOnSheets() {
    // let propcount = 0;
    let filesPath = defaultProps.locale == "en_CA" ? enFilePath : frFilePath;
    console.log("\x1b[32m Process for " + defaultProps.locale, "\x1b[0m", "\x1b[37m" + JSON.stringify(filesPath) + "\x1b[0m");
    console.log("");
    if (fs.existsSync(filesPath.categoryPath) && fs.readdirSync(filesPath.categoryPath).length > 0) {
        let a = await setProductCategoryVal(filesPath.categoryPath);
        fs.writeFileSync(path.resolve("output/" + defaultProps.locale + "productscategory.txt"), JSON.stringify(productcategory));
        console.log("products category mapping values text file created");
        console.log("");
    } else {
        console.log("\x1b[33m" + filesPath.categoryPath + " not exist or empty...\x1b[0m");
    }

    if (fs.existsSync(filesPath.productMasterPath) && fs.readdirSync(filesPath.productMasterPath).length > 0) {
        let b = await setProductsVal(filesPath.productMasterPath);
        fs.writeFileSync(path.resolve("output/" + defaultProps.locale + "products.txt"), "[ " + products.join(", ") + " ]");
        console.log("products text file created");
        console.log("");
    } else {
        console.log("\x1b[33m" + filesPath.productMasterPath + " not exist or empty...\x1b[0m");
    }

    if (defaultProps.locale == "en_CA") {
        if (fs.existsSync(frFilePath.productPropsPath) && fs.readdirSync(frFilePath.productPropsPath).length > 0) {
            let d = await readNameProperty(frFilePath.productPropsPath);
        } else {
            console.log("\x1b[33m" + frFilePath.productPropsPath + " not exist or empty...\x1b[0m");
        }
    } else if (defaultProps.locale == "fr_CA") {
        if (fs.existsSync(enFilePath.productPropsPath) && fs.readdirSync(enFilePath.productPropsPath).length > 0) {
            let d = await readNameProperty(enFilePath.productPropsPath);
        } else {
            console.log("\x1b[33m" + enFilePath.productPropsPath + " not exist or empty...\x1b[0m");
        }
    }
    fs.writeFileSync(path.resolve("output/" + defaultProps.locale + "altlangnameproperty.txt"), JSON.stringify(altLangNameProperty));
    console.log("name property added...");

    if (fs.existsSync(filesPath.productPropsPath) && fs.readdirSync(filesPath.productPropsPath).length > 0) {
        let c = await setProductPropsVal(filesPath.productPropsPath);
        fs.writeFileSync(path.resolve("output/" + defaultProps.locale + "props.txt"), JSON.stringify(productProps));
        console.log("props values text file created");
        console.log("");
    } else {
        console.log("\x1b[33m" + filesPath.productPropsPath + " not exist or empty...\x1b[0m");
    }

    // if (index == keysLength) {
    //     resolve(true);
    //     console.log('\x1b[34mRead process ended, values are set to populate...\x1b[0m');
    // }

    // index++;
}


function setProductCategoryVal(dirPath) {
    return new Promise(resolve => {
        let keysLength = totalcount(dirPath);
        let index = 1;
        fs.readdirSync(dirPath).forEach(fileName => {
            if (fileName.endsWith(".xlsx")) {
                let tempbook = new excel.Workbook();
                tempbook.xlsx.readFile(path.join(dirPath, fileName)).then(book => {
                    let productCategorySheet = book.getWorksheet("productcategory");
                    if (typeof productCategorySheet != "undefined") {
                        productCategorySheet.spliceRows(1, 1);
                        productCategorySheet.eachRow((row, rowno) => {
                            let categoryid = row.getCell(1).value.toString();
                            let productid = row.getCell(2).value.toString();
                            productcategory[productid] = categoryid;
                        });
                    }
                    console.log(fileName);
                    if (index == keysLength) {
                        resolve(true);
                        console.log('\x1b[34mProduct Category read process ended...\x1b[0m');
                    }
                    index++;
                });
            } else index++;
        });
    });
}

function setProductsVal(dirPath) {
    return new Promise(resolve => {
        let keysLength = totalcount(dirPath);
        let index = 1;
        fs.readdirSync(dirPath).forEach(fileName => {

            if (fileName.endsWith(".xlsx")) {
                let tempbook = new excel.Workbook();
                tempbook.xlsx.readFile(path.join(dirPath, fileName)).then(book => {
                    let productMasterSheet = book.getWorksheet("productmaster");

                    if (typeof productMasterSheet != "undefined") {
                        productMasterSheet.spliceRows(1, 1);
                        productMasterSheet.eachRow((row, rowno) => {
                            let productid = row.getCell(1).value.toString();
                            products.push(productid);
                        });
                    }
                    console.log(fileName);
                    if (index == keysLength) {
                        resolve(true);
                        console.log('\x1b[34mProducts read process ended...\x1b[0m');
                    }
                    index++;
                });
            } else index++;
        });
    });
}

function readNameProperty(dirPath) {
    return new Promise(resolve => {
        let keysLength = totalcount(dirPath);
        let index = 1;
        fs.readdirSync(dirPath).forEach(fileName => {
            if (fileName.endsWith(".xlsx")) {
                let tempbook = new excel.Workbook();
                tempbook.xlsx.readFile(path.join(dirPath, fileName)).then(book => {
                    let productPropsSheet = book.getWorksheet(
                        "productmasterproperties"
                    );
                    if (typeof productPropsSheet != "undefined") {
                        productPropsSheet.spliceRows(1, 1);
                        productPropsSheet.eachRow((row, rowno) => {
                            let productid = row.getCell(1).value.toString();
                            let facetname = row.getCell(2).value.toString();
                            let facetvalue = row.getCell(3).value.toString();
                            if (facetname == "name") {
                                altLangNameProperty[productid] = facetvalue;
                            }
                        });
                    }
                    console.log(fileName);
                    if (index == keysLength) {
                        resolve(true);
                        console.log('\x1b[34mProduct props read process ended...\x1b[0m');
                    }
                    index++;
                });
            } else index++;
        });
    });
}


function setProductPropsVal(dirPath) {
    return new Promise(resolve => {
        let keysLength = totalcount(dirPath);
        let index = 1;
        let altLang = (defaultProps.locale == "en_CA") ? "fr_CA" : "en_CA";
        fs.readdirSync(dirPath).forEach(fileName => {
            if (fileName.endsWith(".xlsx")) {
                let tempbook = new excel.Workbook();
                tempbook.xlsx.readFile(path.join(dirPath, fileName)).then(book => {
                    let productPropsSheet = book.getWorksheet(
                        "productmasterproperties"
                    );
                    if (typeof productPropsSheet != "undefined") {
                        productPropsSheet.spliceRows(1, 1);
                        productPropsSheet.eachRow((row, rowno) => {
                            let productid = row.getCell(1).value.toString();
                            let facetname = row.getCell(2).value.toString();
                            let facetvalue = row.getCell(3).value.toString();

                            if (products.indexOf(productid) != -1) {

                                if (facetname == "alternatelanguagerecipes" && defaultProps.isAltLangNeed) {
                                    let altlangRecipeJson = JSON.parse(facetvalue)[0];
                                    let recipeJson = {};
                                    if (typeof altLangNameProperty[altlangRecipeJson.RecipeId] != "undefined") {
                                        // altlangRecipeJson.RecipeName = altLangNameProperty[altlangRecipeJson.RecipeId];
                                        recipeJson.name = altLangNameProperty[altlangRecipeJson.RecipeId];
                                    }
                                    recipeJson.id = altlangRecipeJson.RecipeId;
                                    recipeJson.languageId = altlangRecipeJson.LanguageId;
                                    recipeJson.siteId = altlangRecipeJson.SiteId;
                                    facetname = "alternateLanguage";
                                    facetvalue = '{"' + altLang + '":' + JSON.stringify(recipeJson) + '}';
                                }

                                if (Object.keys(productProps).indexOf(productid) != -1) {
                                    let propObj = productProps[productid];
                                    if (typeof propObj[facetname] != "undefined") {
                                        propObj[facetname] = propObj[facetname] + "," + facetvalue;
                                    } else {
                                        propObj[facetname] = facetvalue;
                                    }
                                    productProps[productid] = propObj;
                                } else {
                                    if (typeof defaultProps.additionalProductProperties != "undefined") {
                                        productProps[productid] = {
                                            ...defaultProps.additionalProductProperties,
                                            [facetname]: facetvalue
                                        };
                                    } else {
                                        productProps[productid] = {
                                            [facetname]: facetvalue
                                        };
                                    }
                                    // propcount++;
                                    // console.log(propcount);
                                }

                                // if(Object.keys(recipeJson).length > 0) {
                                //     productProps[productid]["alternateLanguage"] = '{"' + altLang + '":' + JSON.stringify(recipeJson) + '}';
                                //     recipeJson = {};
                                // }

                            } else {
                                // console.log(fileName + " - " + productid);
                            }
                        });
                    }
                    console.log(fileName);
                    if (index == keysLength) {
                        resolve(true);
                        console.log('\x1b[34mProduct props read process ended...\x1b[0m');
                    }
                    index++;
                });
            } else index++;
        });
    });
}

function generateProductPropsSheet() {
    var productPropsSheet = initProductPropsSheet();
    var catalogProductPropsSheet = initCatalogProductPropsSheet();

    let catalogid = defaultProps.locale == "en_CA" ? defaultProps.catalogid.en : defaultProps.catalogid.fr;
    let fileSavePath = defaultProps.locale == "en_CA" ? "output/en/" : "output/fr/";

    if (typeof defaultProps.productPropsRangePerSheet == "undefined")
        defaultProps.productPropsRangePerSheet = 1000;

    let recipeCount = 0;
    let rangeCount = 0;
    Object.keys(productProps).forEach((id) => {

        Object.keys(productProps[id]).forEach((facet) => {
            if (facet == "grin" || facet == "languageid" || facet == "countryid" || facet == "recipeindustrysector") {
                productPropsSheet.addRow([
                    id, facet, productProps[id][facet], "false", defaultProps.locale, "true", "", ""
                ]);
            }

            catalogProductPropsSheet.addRow([
                catalogid, id, facet, productProps[id][facet], "false", defaultProps.locale, "", ""
            ]);

        });

        if (rangeCount == defaultProps.productPropsRangePerSheet) {
            // console.log(rangeCount);
            productPropsWorkbook.xlsx.writeFile(path.resolve(fileSavePath + defaultProps.locale + "_Product_Props_with_Catalog_" + "_" + (recipeCount - rangeCount) + "_" + recipeCount + "_" + startTime + ".xlsx"))
                .then((res) => {}).catch(err => console.log(err));
            rangeCount = 0;
            productPropsWorkbook = null;
            productPropsWorkbook = new excel.Workbook();
            // productPropsSheet = initProductPropsSheet();
            catalogProductPropsSheet = initCatalogProductPropsSheet();
            console.log(fileSavePath + defaultProps.locale + "_Product_Props_with_Catalog_" + (recipeCount - rangeCount) + "_" + recipeCount + "_" + startTime + ".xlsx created...");
        }

        recipeCount++;
        rangeCount++;
    });

    productPropsWorkbook.xlsx.writeFile(path.resolve(fileSavePath + defaultProps.locale + "_Product_Props_with_Catalog_" + (recipeCount - rangeCount) + "_" + recipeCount + "_" + startTime + ".xlsx"))
        .then((res) => {});
    console.log(fileSavePath + defaultProps.locale + "_Product_Props_with_Catalog_" + (recipeCount - rangeCount) + "_" + recipeCount + "_" + startTime + ".xlsx created...");

    workbook.xlsx.writeFile(path.resolve(fileSavePath + defaultProps.locale + "_Product_Props_For_Master_" + recipeCount + "_" + startTime + ".xlsx"))
        .then((res) => {});
    console.log(fileSavePath + defaultProps.locale + "_Product_Props_For_Master_" + recipeCount + "_" + startTime + ".xlsx created...");

}

function generateProductSkuSheets() {
    const productMasterSheet = initProductMasterSheet();
    const catalogProductMasterSheet = initCatalogProductSheet();

    const skuMasterSheet = initSkuMasterSheet();
    const catalogSkuSheet = initCatalogSkuSheet();

    let catalogid = defaultProps.locale == "en_CA" ? defaultProps.catalogid.en : defaultProps.catalogid.fr;
    let defaultCategoryId = defaultProps.locale == "en_CA" ? defaultProps.defaultCategoryId.en : defaultProps.defaultCategoryId.fr;
    let fileSavePath = defaultProps.locale == "en_CA" ? "output/en/" : "output/fr/";

    products.forEach((id) => {
        productMasterSheet.addRow([
            id, "1", "false", "", "", "", "false", "", "", "", typeof productcategory[id] != "undefined" ? productcategory[id] : defaultCategoryId, "", "", defaultProps.startTime, defaultProps.endTime, "true", "false", ""
        ]);

        catalogProductMasterSheet.addRow([
            catalogid, id, "1", "false", "true", typeof productcategory[id] != "undefined" ? productcategory[id] : defaultCategoryId, defaultProps.startTime, defaultProps.endTime, ""
        ]);

        skuMasterSheet.addRow([
            id, id, id, "1", "false", defaultProps.startTime, defaultProps.endTime
        ]);

        catalogSkuSheet.addRow([
            catalogid, id, "1", "false", defaultProps.startTime, defaultProps.endTime
        ]);

    });

    workbook.xlsx.writeFile(path.resolve(fileSavePath + defaultProps.locale + "_Products_Sku_with_Catalog_" + startTime + ".xlsx"))
        .then((res) => {});
    console.log(fileSavePath + defaultProps.locale + "_Products_Sku_with_Catalog_" + startTime + ".xlsx created...")
}

//Initiate productmaster sheet
function initProductMasterSheet() {
    const productMasterSheet = workbook.addWorksheet("productmaster");
    productMasterSheet.addRow([
        "productid", "status", "collection", "subproductids", "upsellproductids", "crosssellproductids", "bundle", "bundlemainproductid", "bundlemandatoryproductids", "bundleoptionalproductids", "defaultparentcategoryid", "groupid", "facetgroup", "starttime", "endtime", "visible", "locked", "segments"
    ]);
    return productMasterSheet;
}

//Initiate catalogproduct sheet
function initCatalogProductSheet() {
    const catalogProductSheet = workbook.addWorksheet("catalogproduct");
    catalogProductSheet.addRow([
        "catalogid", "productid", "status", "locked", "visible", "defaultparentcategoryid", "starttime", "endtime", "segments"
    ]);
    return catalogProductSheet;
}

//Initiate productmasterproperties sheet
function initProductPropsSheet() {
    const productPropsSheet = workbook.addWorksheet("productmasterproperties");
    productPropsSheet.addRow([
        "productid", "name", "value", "locked", "locale", "overridebysku", "starttime", "endtime"
    ]);
    return productPropsSheet;

}

//Initiate catalogproductproperties sheet
function initCatalogProductPropsSheet() {
    const catalogProductPropsSheet = productPropsWorkbook.addWorksheet("catalogproductproperties");
    catalogProductPropsSheet.addRow([
        "catalogid", "productid", "name", "value", "locked", "locale", "starttime", "endtime"
    ]);
    return catalogProductPropsSheet;

}

//Initiate skumaster sheet
function initSkuMasterSheet() {
    const skuMasterSheet = workbook.addWorksheet("skumaster");
    skuMasterSheet.addRow([
        "skuid", "productids", "upcids", "status", "locked", "starttime", "endtime"
    ]);
    return skuMasterSheet;
}

//Initiate catalogsku sheet
function initCatalogSkuSheet() {
    const catalogSkuSheet = workbook.addWorksheet("catalogsku");
    catalogSkuSheet.addRow([
        "catalogid", "skuid", "status", "locked", "starttime", "endtime"
    ]);
    return catalogSkuSheet;
}